#!/bin/bash

# Copyright 2020 Patrick Nagel
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

echo "== Merge GoPro MP4s =="
echo

if [[ "${1}" == "" ]] || [[ "${1}" == "-h" ]] || [[ "${1}" == "--help" ]]; then
  echo "Syntax: $0 <output directory> [<start date/time>]
  
This script looks for GoPro .MP4 files in the current directory
and seamlessly concatenates them using ffmpeg into one file per recorded
video. The resulting single .mp4 file is written to the output directory
specified as first argument.

As an optional second parameter, a date/time can be provided. The script
will then only process videos that have been recorded on/after that
date/time. This is useful when you want to skip some older videos that
you have not yet deleted from the SD card.

Typical workflow:
- Take MicroSD card out of the GoPro and put it into the SD card adapter
- Insert SD card into computer
- Open terminal and change directory to SD card mount point
- Run this script and provide a directory on the computer's hard
  drive as parameter
- Use any video processing application to work on the merged file(s)"
  exit 0
fi

if [ ! -d "${1}" ]; then
  echo "${1} is not an existing directory."
  exit 1
else
  targetdir="${1}"
  echo "Target directory: ${1}"
fi

if ! ffmpeg -version >/dev/null; then
  echo "ffmpeg not found, make sure that it is installed."
  exit 1
fi

if [[ "${2}" == "" ]]; then
  startdate="1970-01-01"
else
  startdate="${2}"
  if ! date -d "${startdate}" >/dev/null; then
    echo "${startdate} is not a valid date."
    exit 1
  fi
  LC_TIME="en_US.UTF-8"
  echo "Considering only files from $(date -d "${startdate}") onwards..."
  echo
fi

IFS='
'
findpattern='GX*.MP4'
declare -a filelist videonumbers

echo "File list:"
filelist=( $(find -maxdepth 1 -newermt "${startdate}" -name "${findpattern}") )
printf '%s\n' "${filelist[@]}"

echo

echo "Video numbers:"
videonumbers=( $(find -maxdepth 1 -newermt "${startdate}" -name "${findpattern}" | sed -e 's|.*GX[A-Z0-9][A-Z0-9]\(....\)\.MP4$|\1|' | sort -u) )
printf '%s\n' "${videonumbers[@]}"

echo
echo "Press Enter to proceed, Ctrl-C to cancel."
read

echo "Processing videos..."
for videonumber in ${videonumbers[@]}; do
  filelistname=$(mktemp -p ${PWD})
  echo "- Creating temporary ffmpeg file list for video ${videonumber}..."
  find -maxdepth 1 -newermt "${startdate}" -name "GX*${videonumber}.MP4" | sort | sed -e's|^./|file |' >${filelistname}
  echo "- Running ffmpeg..."
  ffmpeg -n -f concat -i ${filelistname} -c copy "${targetdir}/GX${videonumber}.mp4" || {
    echo "ffmpeg failed (see above)."
    rm -f ${filelistname}
    exit 1;
  }
  rm -f ${filelistname}
  echo "- Taking over timestamp..."
  touch -r "GX01${videonumber}.MP4" "${targetdir}/GX${videonumber}.mp4"
done

echo
echo "Resulting (merged) file(s):"
for videonumber in ${videonumbers[@]}; do
  ls -lh "${targetdir}/GX${videonumber}.mp4"
done
